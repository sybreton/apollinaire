from importlib.metadata import version

__version__ = version ('apollinaire')

import apollinaire.songlib

import apollinaire.psd

import apollinaire.processing

import apollinaire.synthetic

import apollinaire.peakbagging

import apollinaire.peakbagging.test_data

import apollinaire.timeseries

