The synthetic module
********************

This submodule has been designed to provide tool
to create synthetic PSD. 

.. autofunction:: apollinaire.synthetic.create_synthetic_psd 

