Installation
************

With pip
########

You can install the package with *pip*: 

``pip install apollinaire``

With conda
##########

If you are more of a **conda** user, **apollinaire** is also available
on **conda-forge**:

``conda install -c conda-forge apollinaire``

From GitLab
###########

If you are interested on the latest developments of the code, you can 
clone the `git repository <https://gitlab.com/sybreton/apollinaire>`_
with the following instruction:

``git clone https://gitlab.com/sybreton/apollinaire.git``

Known issues 
############

Versions older than **1.3.1** are not compatible with ``corner==2.2.2``. 
If you still want to use one of these versions, you should use an older version
of ``corner``.


