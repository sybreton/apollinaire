Citing **apollinaire**
######################

If you use **apollinaire** in one of your publications, please cite
the reference paper for the module, `Breton et al. (2022a)
<https://ui.adsabs.harvard.edu/abs/2022A%26A...663A.118B/abstract>`_.

.. parsed-literal::

  @ARTICLE{2022A&A...663A.118B,
       author = {{Breton}, S.~N. and {Garc{\'\i}a}, R.~A. and {Ballot}, J. and {Delsanti}, V. and {Salabert}, D.},
        title = "{Deciphering stellar chorus: apollinaire, a Python 3 module for Bayesian peakbagging in helioseismology and asteroseismology}",
      journal = {\aap},
     keywords = {Sun: helioseismology, asteroseismology, stars: oscillations, methods: data analysis, stars: solar-type, Astrophysics - Solar and Stellar Astrophysics, Astrophysics - Instrumentation and Methods for Astrophysics},
         year = 2022,
        month = jul,
       volume = {663},
          eid = {A118},
        pages = {A118},
          doi = {10.1051/0004-6361/202243330},
  archivePrefix = {arXiv},
       eprint = {2202.07524},
  primaryClass = {astro-ph.SR},
       adsurl = {https://ui.adsabs.harvard.edu/abs/2022A&A...663A.118B},
      adsnote = {Provided by the SAO/NASA Astrophysics Data System}
  }


The module also has a `code record <https://ascl.net/2306.022>`_ 
in the Astrophysics Source Code Library:
 
.. parsed-literal::

  @MISC{2023ascl.soft06022B,
         author = {{Breton}, S.~N. and {Garc{\'\i}a}, R.~A. and {Ballot}, J. and {Delsanti}, V. and {Salabert}, D.},
          title = "{apollinaire: Helioseismic and asteroseismic peakbagging frameworks}",
       keywords = {Software},
   howpublished = {Astrophysics Source Code Library, record ascl:2306.022},
           year = 2023,
          month = jun,
            eid = {ascl:2306.022},
          pages = {ascl:2306.022},
  archivePrefix = {ascl},
         eprint = {2306.022},
         adsurl = {https://ui.adsabs.harvard.edu/abs/2023ascl.soft06022B},
        adsnote = {Provided by the SAO/NASA Astrophysics Data System}
  }

If you used some functions of the **songlib** submodule, please cite also
`Breton et al. (2022b) 
<https://ui.adsabs.harvard.edu/abs/2022A%26A...658A..27B/abstract>`_.

.. parsed-literal::

  @ARTICLE{2022A&A...658A..27B,
         author = {{Breton}, S.~N. and {Pall{\'e}}, P.~L. and {Garc{\'\i}a}, R.~A. and {Fredslund Andersen}, M. and {Grundahl}, F. and {Christensen-Dalsgaard}, J. and {Kjeldsen}, H. and {Mathur}, S.},
          title = "{No swan song for Sun-as-a-star helioseismology: Performances of the Solar-SONG prototype for individual mode characterisation}",
        journal = {\aap},
       keywords = {Sun: helioseismology, methods: data analysis, Astrophysics - Solar and Stellar Astrophysics, Astrophysics - Instrumentation and Methods for Astrophysics},
           year = 2022,
          month = feb,
         volume = {658},
            eid = {A27},
          pages = {A27},
            doi = {10.1051/0004-6361/202141496},
  archivePrefix = {arXiv},
         eprint = {2110.12698},
   primaryClass = {astro-ph.SR},
         adsurl = {https://ui.adsabs.harvard.edu/abs/2022A&A...658A..27B},
        adsnote = {Provided by the SAO/NASA Astrophysics Data System}
  }

You should also cite the following references as they correspond to modules
abundantly used by **apollinaire**: 
`numpy <https://numpy.org/citing-numpy/>`_, 
`pandas <https://pandas.pydata.org/about/citing.html>`_, 
`matplotlib <https://matplotlib.org/stable/users/project/citing.html>`_, 
`scipy <https://scipy.org/citing-scipy/>`_, 
`corner <https://corner.readthedocs.io/en/latest/>`_,
`astropy <https://www.astropy.org/acknowledging.html>`_, 
`h5py <http://citebay.com/how-to-cite/h5py/>`_,
`george <https://george.readthedocs.io/en/latest/>`_ 
and, of course `emcee <https://ui.adsabs.harvard.edu/abs/2013PASP..125..306F/exportcitation>`_.
